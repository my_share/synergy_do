package module_1.lesson_1;

public class Main {
    public static void main(String[] args) {
        //1. Выведите ваше имя в консоль
        System.out.println("----------- 1. Выведите ваше имя в консоль------------");
        System.out.println("Vasiliy");

        //2. Выведите любимое стихотворение в консоль
        System.out.println("----------- 2. Выведите любимое стихотворение в консоль------------");
        System.out.println("Белая берёза");
        System.out.println("Под моим окном");
        System.out.println("Принакрылась снегом,");
        System.out.println("Точно серебром.");

        //4. Вывести значение 2 + 2 * 2
        System.out.println("----------- Вывести значение 2 + 2 * 2 ------------");
        System.out.println(2 + 2 * 2);

        //5. Вывести значение (2+2)*2
        System.out.println("----------- 5. Вывести значение (2+2)*2 ------------");
        System.out.println((2 + 2) * 2);

        //6. Вывести значение деления 100 на пи
        System.out.println("----------- 6. Вывести значение деления 100 на пи ------------");
        System.out.println(100.0 / 3.14);

        //7. Вывести значение 12345 в третьей степени
        System.out.println("----------- 7. Вывести значение 12345 в третьей степени ------------");
        System.out.println(Math.pow(12345.0, 3.0));

        //8. Вывести квадратный корень от двух в 10 степени
        System.out.println("----------- 8. Вывести квадратный корень от двух в 10 степени ------------");
        System.out.println(Math.pow(Math.sqrt(2.0), 10.0));

        //9. Вывести корень из двух, возведенный в 10 степень
        System.out.println("----------- 9. Вывести корень из двух, возведенный в 10 степень ------------");
        System.out.println(Math.sqrt(Math.pow(2.0, 10.0)));

        //10. Что будет, если в джаве поделить на ноль? Проверить
        System.out.println("----------- 10. Что будет, если в джаве поделить на ноль? Проверить ------------");
//        System.out.println(10/0);
        System.out.println("exception");

        //11. Попробуйте сложить две строки в Java. Выведите результат.
        System.out.println("----------- 11. Попробуйте сложить две строки в Java. Выведите результат. ------------");
        System.out.println("строка 1 + " + "строка 2" + "= конкатенация строк");

        //12. Попробуйте вычесть, разделить две строки.
        System.out.println("----------- 12. Попробуйте вычесть, разделить две строки. ------------");
        System.out.println("строки делить нельзя");

        //13. Попробуйте сложить строку с числом пи. Что получилось?
        System.out.println("----------- 13. Попробуйте сложить строку с числом пи. Что получилось? ------------");
        System.out.println("компилятор приведёт число 'пи' к string");

        Main obj = new Main();
        //14. Полезное упражнение: напишите программу, которая считает корень линейного уравнения ax+b=0
        System.out.println("----------- 14. Полезное упражнение: напишите программу, которая считает корень линейного уравнения ax+b=0 ------------");
        obj.sqrtLineEquation();

        //15. Полезное упражнение: напишите программу, которая считает корни уравнения (ax+b)*(cx+d)=0
        System.out.println("----------- 15. Полезное упражнение: напишите программу, которая считает корни уравнения (ax+b)*(cx+d)=0 ------------");
        obj.sqrtEquation();

        //16. Полезное упражнение: напишите программу, которая считает дискриминант квадратного уравнения
        System.out.println("----------- 16. Полезное упражнение: напишите программу, которая считает дискриминант квадратного уравнения ------------");
        obj.quadraticEquation();
    }

    /**
     * 14. Полезное упражнение: напишите программу, которая считает корень линейного уравнения ax+b=0
     */
    public void sqrtLineEquation() {
        double a = 4, b = 24;
        System.out.println(Math.sqrt((b / a)));
    }

    /**
     * 15. Полезное упражнение: напишите программу, которая считает корни уравнения (ax+b)*(cx+d)=0
     */
    public void sqrtEquation() {
        double D, b, a, c;
        a = 2;
        b = -3;
        c = -5;
        D = Math.pow(b, 2) - 4 * a * c;
        System.out.println("дискриминант квадратного уравнения = " + D);
        if (D < 0) {
            System.out.println("Уравнение не имеет корней");
        } else if (D == 0) {
            System.out.println((-(b / 2)) / a);
        } else {
            System.out.println("x_1 = " + ((-b - Math.sqrt(D)) / (2 * a)));
            System.out.println("x_2 = " + ((-b + Math.sqrt(D)) / (2 * a)));
        }
    }

    /**
     * 16. Полезное упражнение: напишите программу, которая считает дискриминант квадратного уравнения
     */
    public void quadraticEquation() {
        double b, a, c;
        a = 3;
        b = 7;
        c = 18;
        System.out.println("дискриминант квадратного уравнения = " + (Math.pow(b, 2) - 4 * a * c));
    }

}
