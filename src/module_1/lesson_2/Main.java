package module_1.lesson_2;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        System.out.println("----------- 1. Если 5 в 15 степени больше миллиарда, вывести - «Степень это мощь. Power is a power.» ------------");
        task_1();
        System.out.println("----------- 2. Задайте переменную. Если она больше 0, вывести «позитив», если меньше 0, вывести «отрицательно» ------------");
        task_2();
        System.out.println("----------- 3. Если квадратный корень из 15 миллионов меньше 5 тысяч, вывести - «два измерения лучше, чем одно» ------------");
        task_3();
        System.out.println("----------- 4. Если 2 в 10 степени меньше 512 вывести - «Pentium 2», иначе вывести - «ARM» ------------");
        task_4();
        System.out.println("----------- 5. Задать две дробных переменных. Вывести наибольшую из них. ------------");
        task_5();
        System.out.println("----------- 6. Задать две переменных - first и second. Вывести first в степени second, second в степени first. ------------");
        task_6();
        System.out.println("----------- 7. Задать две переменных - икс и игрек. Вывести, что больше - икс в степени игрек, или наоборот. ------------");
        task_7();
        System.out.println("----------- 8. Вывести числа от 1 до 100. ------------");
        task_8();
        System.out.println("----------- 9. Вывести числа от 50 до 100 ------------");
        task_9();
        System.out.println("----------- 10. Вывести числа от 100 до 1 ------------");
        task_10();
        System.out.println("----------- 11. Вывести числа от 0 до -100 ------------");
        task_11();
        System.out.println("----------- 12. Задать строковую переменную. Заменить в ней все буквы о на «обро» ------------");
        task_12();
        System.out.println("----------- 13. Задать строковую переменную. Вывести ее в верхнем регистре. ------------");
        task_13();
        System.out.println("----------- 14. Задать строковую переменную. Заменить в ней буквы а на @, а буквы о на 0. ------------");
        task_14();
        System.out.println("----------- 15. Задать две строковых переменных. Найти, какая из них длиннее. (Используйте .length() ) ------------");
        task_15();
        System.out.println("----------- 16. Задать три переменных, найти наибольшую из них ------------");
        task_16();
        System.out.println("----------- 17. Напишите программу, сохраняющую в файл статью из википедии «Проблема 2000 года». Прочитайте её. ------------");
        task_17();
        System.out.println("----------- 18. Напишите программу, сохраняющую в файл статью из википедии «Дональд Кнут». Перед сохранением в файл замените все слова Кнут на Пряник ------------");
        task_18();
        System.out.println("----------- 19. Напишите программу, которая сохраняет в файл случайную статью из Википедии. ------------");
        task_19();
        System.out.println("----------- 20. Сложная. Напишите программу, которая сохраняет в разные файлы 50 случайных статей из Википедии ------------");
        task_20();
    }

    /**
     * 1.Если 5 в 15 степени больше миллиарда, вывести - «Степень это мощь. Power is a power.»
     */
    public static void task_1() {
        if (Math.pow(5, 15) > 1000000000) {
            System.out.println("Степень это мощь. Power is a power.");
        }
    }

    /**
     * 2. Задайте переменную. Если она больше 0, вывести «позитив», если меньше 0, вывести «отрицательно»
     */
    public static void task_2() {
        int a = 5;
        if (a > 0) {
            System.out.println("позитив");
        } else {
            System.out.println("отрицательно");
        }
    }

    /**
     * 3. Если квадратный корень из 15 миллионов меньше 5 тысяч, вывести - «два измерения лучше, чем одно»
     */
    public static void task_3() {
        if (Math.sqrt(15000000) < 5000) {
            System.out.println("два измерения лучше, чем одно");
        }
    }

    /**
     * 4. Если 2 в 10 степени меньше 512 вывести - «Pentium 2», иначе вывести - «ARM»
     */
    public static void task_4() {
        if (Math.pow(2, 10) < 512) {
            System.out.println("Pentium 2");
        } else {
            System.out.println("ARM");
        }
    }

    /**
     * 5. Задать две дробных переменных. Вывести наибольшую из них.
     */
    public static void task_5() {
        double first = 2.5;
        double second = 6.4;
        if (first > second) {
            System.out.println("переменная first > second");
        } else if (first < second) {
            System.out.println("переменная second > first");
        }
    }

    /**
     * 6. Задать две переменных - first и second. Вывести first в степени second, second в степени first.
     */
    public static void task_6() {
        int first = 2;
        int second = 6;
        System.out.println("first в степени second - " + Math.pow(first, second));
        System.out.println("second в степени first - " + Math.pow(second, first));
    }

    /**
     * 7. Задать две переменных - икс и игрек. Вывести, что больше - икс в степени игрек, или наоборот.
     */
    public static void task_7() {
        int x = 2;
        int y = 6;
        double xy = Math.pow(x, y);
        double yx = Math.pow(y, x);
        if (xy > yx) {
            System.out.println("икс в степени игрек (" + xy + ") > (" + yx + ") игрек в степен икс");
        } else if (yx < xy) {
            System.out.println("игрек в степен икс (" + yx + ") > (" + xy + ") икс в степени игрек");
        }
    }

    /**
     * 8. Вывести числа от 1 до 100
     */
    public static void task_8() {
        int i = 1;
        while (i <= 100) {
            System.out.print(i + " ");
            i++;
        }
        System.out.println("");
    }

    /**
     * 9. Вывести числа от 50 до 100
     */
    public static void task_9() {
        int i = 50;
        while (i <= 100) {
            System.out.print(i + " ");
            i++;
        }
        System.out.println("");
    }

    /**
     * 10. Вывести числа от 100 до 1
     */
    public static void task_10() {
        int i = 100;
        while (i >= 1) {
            System.out.print(i + " ");
            i--;
        }
        System.out.println("");
    }

    /**
     * 11. Вывести числа от 0 до -100
     */
    public static void task_11() {
        int i = 0;
        while (i >= -100) {
            System.out.print(i + " ");
            i--;
        }
        System.out.println("");
    }

    /**
     * 12. Задать строковую переменную. Заменить в ней все буквы о на «обро»
     */
    public static void task_12() {
        String worl = "Задать строковую переменную";
        System.out.println(worl.replace("о", "ОБРО"));
    }

    /**
     * 13. Задать строковую переменную. Вывести ее в верхнем регистре.
     */
    public static void task_13() {
        String worl = "Задать строковую переменную";
        System.out.println(worl.toUpperCase());
    }

    /**
     * 14. Задать строковую переменную. Заменить в ней буквы а на @, а буквы о на 0.
     */
    public static void task_14() {
        String worl = "Задать строковую переменную";
        worl = worl.replace("а", "@");
        System.out.println(worl.replace("о", "0"));
    }

    /**
     * 15. Задать две строковых переменных. Найти, какая из них длиннее. (Используйте .length() )
     */
    public static void task_15() {
        String first = "Первая строковую переменную";
        String second = "Вторая строковую переменную плюс немного символов";
        int firstLength = first.length();
        int secondLength = second.length();
        if (firstLength > secondLength) {
            System.out.println("Первая строка больше второй строки на " + (firstLength - secondLength));
        } else {
            System.out.println("Вторая строка больше первая строки на " + (secondLength - firstLength));
        }
    }

    /**
     * 16. Задать три переменных, найти наибольшую из них
     */
    public static void task_16() {
        int x = 1, y = 2, z = 3;
        System.out.println("переменные x = " + x + "; y = " + y + "; z = " + z + ";");
        if (x > y && x > z) {
            System.out.println("переменные x = " + x + " больше остальных");
        } else if (y > z) {
            System.out.println("переменные y = " + y + " больше остальных");
        } else {
            System.out.println("переменные z = " + z + " больше остальных");
        }

    }

    /**
     * 17. Напишите программу, сохраняющую в файл статью из википедии «Проблема 2000 года». Прочитайте её.
     */
    public static void task_17() {
        String url = "https://ru.wikipedia.org/wiki/%D0%9F%D1%80%D0%BE%D0%B1%D0%BB%D0%B5%D0%BC%D0%B0_2000_%D0%B3%D0%BE%D0%B4%D0%B0";
        String outputFileName = writterFile("wiki2000.html", getPageSourse(url));
        System.out.println("Cтатью из википедии «Проблема 2000 года» сохранена в файле по следующему пути: " + outputFileName);
        System.out.println(readerFile("wiki2000.html"));
    }

    /**
     * 18. Напишите программу, сохраняющую в файл статью из википедии «Дональд Кнут». Перед сохранением в файл замените все слова Кнут на Пряник
     */
    public static void task_18() {
        String url = "https://ru.wikipedia.org/wiki/%D0%9A%D0%BD%D1%83%D1%82,_%D0%94%D0%BE%D0%BD%D0%B0%D0%BB%D1%8C%D0%B4_%D0%AD%D1%80%D0%B2%D0%B8%D0%BD";
        String text = getPageSourse(url);
        String outputFileName = writterFile("wiki_gingerbread.html", text.replace("Кнут", "Пряник"));
        System.out.println("Cтатью из википедии «Дональд Кнут» с заменой сохранена в файле по следующему пути: " + outputFileName);
    }

    /**
     * 19. Напишите программу, которая сохраняет в файл случайную статью из Википедии.
     */
    public static void task_19() {
        String url = "https://ru.wikipedia.org/wiki/%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BB%D1%83%D1%87%D0%B0%D0%B9%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0";
        String text = getPageSourse(url);
        String strName = text.substring((text.indexOf("<title>")+7),text.indexOf("</title>"))+".html";
        String nameFile = strName.replaceAll("\\s+","");
        String outputFileName = writterFile(nameFile, text);
        System.out.println("Cлучайную статью из Википедии сохранена в файле по следующему пути: " + outputFileName);
    }

    /**
     * 20. Сложная. Напишите программу, которая сохраняет в разные файлы 50 случайных статей из Википедии
     */
    public static void task_20() {
        int i = 1, articles = 50;
        System.out.println("Список файлов к случайным статьям из Википедии:");
        while (articles >= i) {
            String url = "https://ru.wikipedia.org/wiki/%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F:%D0%A1%D0%BB%D1%83%D1%87%D0%B0%D0%B9%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0";
            String text = getPageSourse(url);
            String strName = text.substring((text.indexOf("<title>") + 7), text.indexOf("</title>")) + ".html";
            String nameFile = strName.replaceAll("\\s+", "");
            String outputFileName = writterFile(nameFile, text);
            System.out.println(i + ". " + strName + " " + outputFileName);
            i++;
        }
    }

    /**
     * полученеи web страницы
     * @param url
     * @return
     */
    private static String getPageSourse(String url) {
        StringBuilder result = new StringBuilder();
        String line;

        try {
            URLConnection urlConnection = new URL(url).openConnection();
            urlConnection.addRequestProperty("User-Agent", "Mozilla");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);

            InputStream is = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                result.append(line);
            }
            is.close();
            br.close();
        } catch (Exception e) {
            System.out.println("ошибка!!!" + e.getMessage());
        }
        return result.toString();
    }

    /**
     * запись в файл
     * @param nameFile
     * @param text
     * @return
     */
    private static String writterFile(String nameFile, String text) {
        String outputFileName = "";
        try {
            String strPathDir = new File("").getAbsolutePath() + "/upload";
            Path pathDir = Paths.get(strPathDir);
            outputFileName = pathDir + "/" + nameFile;
            if (!Files.exists(pathDir)) {
                Files.createDirectory(pathDir);
            }
            BufferedWriter writter = new BufferedWriter(new FileWriter(outputFileName));
            writter.write(text);
            writter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputFileName;
    }

    /**
     * чтение файла
     * @param nameFile
     * @return
     */
    private static String readerFile(String nameFile) {
        String text = "";
        try {
            String inputFileName = new File("").getAbsolutePath() + "/upload/" + nameFile;
            Path pathDir = Paths.get(inputFileName);
            if (Files.exists(pathDir)) {
                BufferedReader reader = new BufferedReader(new FileReader(inputFileName));
                String line;
                while ((line = reader.readLine()) != null) {
                    text += line;
                }
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }
}

