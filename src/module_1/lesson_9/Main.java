package module_1.lesson_9;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("-----------  Пользователь вводит три слова, выведите в обратном порядке.  ------------");
        task();
        System.out.println("----------- 1. Пользователь вводит три строки, выведите в обратном порядке. ------------");
        task_1();
        System.out.println("----------- 2. Используя System.in (без сканнера), считайте слово из 5 букв ------------");
        task_2();
        System.out.println("----------- 3. Используя System.in (без сканнера),считайте слово до пробела. Макс размер слова 10 символов. ------------");
        task_3();
        System.out.println("----------- 4.Используя Scanner на основе FileInputStream, прочесть из файла строку:" +
                " название другого файла. Из этого другого файла прочесть название третьего файла, и в третий файл записать букву, которую пользователь введет через System.in (без сканера) ------------");
        task_4();
        System.out.println("----------- 5.Пользователь вводит 7 символов. Используя System.in (без сканнера), считайте целое число до первой нецифры.(пример: «35 руб», ответ 35.) считаем, что первым пользователь всегда вводит цифру. ------------");
        task_5();
        System.out.println("----------- 6.Аналогично предыдущей задаче, но пользователь всегда вводит дробное число (пример ввода: «3.5 кг») ------------");
        task_6();
        System.out.println("----------- 7.Пользователь вводит 10 строк. Те из них, в которых есть восклицательный знак, выведите в stderr. Попробуйте использовать цикл. ------------");
        task_7();
        System.out.println("----------- 8.Пользователь вводит 10 строк, потом число max. Вывести в stdout те строки, длина которых меньше max, а в stderr те, длина которых больше, обрезав по max. ------------");
        task_8();
    }

    /**
     * Пользователь вводит три слова, выведите в обратном порядке.
     */
    public static void task() {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String[] arStr = str.split(" ");
        if (arStr.length < 3) {
            System.out.println("вы вели меньше трех слов");
            return;
        }
        for (int i = 2; i >= 0; i--) {
            System.out.println(arStr[i]);
        }
    }

    /**
     * 1. Пользователь вводит три строки, выведите в обратном порядке.
     */
    public static void task_1() {
        Scanner scanner = new Scanner(System.in);
        String[] arStr = new String[3];
        for (int i = 0; i <= 2; i++) {
            System.out.printf("Введите строку # " + (i + 1) + ": ");
            arStr[i] = scanner.nextLine();
        }
        for (int i = 2; i >= 0; i--) {
            System.out.println(arStr[i]);
        }
    }

    /**
     * 2. Используя System.in (без сканнера), считайте слово из 5 букв
     */
    public static void task_2() {
        try {
            int i = 0;
            while (i <= 4) {
                System.out.print((char) System.in.read());
                i++;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 3. Используя System.in (без сканнера),считайте слово до пробела. Макс размер слова 10 символов.
     */
    public static void task_3() {
        try {
            char[] arChar = new char[10];
            int i = 0;
            while (i <= 9) {
                arChar[i] = (char) System.in.read();
                if ((int) arChar[i] == 32) {
                    break;
                }
                i++;
            }
            System.out.println(String.valueOf(arChar));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 4. Используя Scanner на основе FileInputStream, прочесть из файла строку: название другого файла.
     * Из этого другого файла прочесть название третьего файла, и в третий файл записать букву, которую пользователь введет через System.in (без сканера)
     */
    public static void task_4() {

        try {
            System.out.printf("Введите  букву: ");
            InputStream symbolStream = System.in;
            char symbol = (char) symbolStream.read();
            InputStream streamOne = new FileInputStream("uploud/file_one.txt");
            Scanner nameFileForRead = new Scanner(streamOne);
            String nameFileTwo = nameFileForRead.nextLine();
            InputStream streamTwo = new FileInputStream("uploud/" + nameFileTwo);
            Scanner nameFileForReadTwo = new Scanner(streamTwo);
            String nameFileWrite = nameFileForReadTwo.nextLine();
            FileWriter fileWriter = new FileWriter("uploud/" + nameFileWrite);
            fileWriter.write(symbol);
            fileWriter.close();
            System.out.println("Буква сохранена в файле по адресу uploud/" + nameFileWrite);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 5. Пользователь вводит 7 символов. Используя System.in (без сканнера), считайте целое число до первой нецифры.
     * (пример: «35 руб», ответ 35.) считаем, что первым пользователь всегда вводит цифру.
     */
    public static void task_5() {
        try {
            System.out.printf("Введите  7 символов: ");
            char[] arChar = new char[7];
            int i = 0;
            while (i <= 7) {
                arChar[i] = (char) System.in.read();
                if ((int) arChar[i] < 48 || (int) arChar[i] > 57) {
                    break;
                }

                i++;
            }
            System.out.println(String.valueOf(arChar));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 6. Аналогично предыдущей задаче, но пользователь всегда вводит дробное число (пример ввода: «3.5 кг»)
     */
    public static void task_6() {

    }

    /**
     * 7. Пользователь вводит 10 строк. Те из них, в которых есть восклицательный знак, выведите в stderr. Попробуйте использовать цикл.
     */
    public static void task_7() {
        Scanner scanner = new Scanner(System.in);
        String str = "";
        for (int i = 0; i <= 9; i++) {
            System.out.printf("Введите строку # " + (i + 1) + ": ");
            str = scanner.nextLine();
            if (str.lastIndexOf("!") > 0) {
                System.out.println("stderr -- " + str);
            }
        }
    }

    /**
     * 8. Пользователь вводит 10 строк, потом число max. Вывести в stdout те строки, длина которых меньше max, а в stderr те, длина которых больше, обрезав по max.
     */
    public static void task_8() {
        Scanner scanner = new Scanner(System.in);
        int lengthArr = 10;
        int max;
        String[] arStr = new String[lengthArr];
        String stdout = "", stderr = "";
        for (int i = 0; i <= (lengthArr - 1); i++) {
            System.out.printf("Введите строку # " + (i + 1) + ": ");
            arStr[i] = scanner.nextLine();
        }
        System.out.printf("Введите число max: ");
        max = scanner.nextInt();
        for (String str : arStr) {
            if (str.length() > max) {
                stdout = stdout.concat("\n" + str);
            } else {
                stderr = stderr.concat("\n" + str);
            }
        }
        System.out.println("-------------stdout-------------");
        System.out.println(stdout);
        System.out.println("-------------stderr-------------");
        System.out.println(stderr);
    }

}

