package module_1.lesson_5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("----------- 1. Пользователь вводит два числа. Возвести второе в степень первого ------------");
        task_1();
        System.out.println("----------- 2. Пользователь вводит три числа. Возвести 1 в степень 2, и результат в степень 3. ------------");
        task_2();
        System.out.println("----------- 3. Решить предыдущую задачу, но Math.pow можно использовать только на одной строчке вашей программы(хоть и много раз) ------------");
        task_3();
        System.out.println("----------- 4. Пользователь вводит, две строки. Найти разницу в длине. ------------");
        task_4();
        System.out.println("----------- 5. Пользователь вводит три строки. Найти разницу в длине самой короткой и самой длинной. ------------");
        task_5();
        System.out.println("----------- 6. Решить предыдущую задачу, не используя операторы >,< итд. ------------");
        task_6();
        System.out.println("----------- 7. Сделать калькулятор для трех чисел: пользователь вводит первое, потом оператор, второе, оператор, третье. Посчитать первое на второе, потом результат с третьим. Пример: 11+4*20 Вывод: 300 ------------");
        task_7();
        System.out.println("----------- 8. Решить предыдущую задачу, но операции считать по приоритету (умножение и деление выше сложения вычитания). Предыдущий пример даст ответ 11 + 80 равно 91 ------------");
        task_8();
        System.out.println("----------- 9. Вывести английский алфавит ------------");
        task_9();
        System.out.println("----------- 10. Пользователь вводит N. Вывести букву английского алфавита по счету N. ------------");
        task_10();
        System.out.println("----------- 11. Вывести случайную букву английского алфавита. ------------");
        task_11();
        System.out.println("----------- 12. Вывести случайное число от 11 до 22 ------------");
        task_12();
    }

    /**
     * 1. Пользователь вводит два числа. Возвести второе в степень первого
     */
    public static void task_1() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите первое число:");
        int firstNamber = scanner.nextInt();
        System.out.printf("Введите второе число:");
        int secondNamber = scanner.nextInt();
        System.out.println("результат второе в степень первого:" + Math.pow(secondNamber, firstNamber));
    }

    /**
     * 2. Пользователь вводит три числа. Возвести 1 в степень 2, и результат в степень 3
     */
    public static void task_2() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите первое число:");
        int firstNamber = scanner.nextInt();
        System.out.printf("Введите второе число:");
        int secondNamber = scanner.nextInt();
        System.out.printf("Введите третие число:");
        int threeNamber = scanner.nextInt();
        double r = Math.pow(firstNamber, secondNamber);
        System.out.println("результат : " + Math.pow(r, threeNamber));
    }

    /**
     * 3. Решить предыдущую задачу, но Math.pow можно использовать только на одной строчке вашей программы (хоть и много раз)
     */
    public static void task_3() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите первое число:");
        int firstNamber = scanner.nextInt();
        System.out.printf("Введите второе число:");
        int secondNamber = scanner.nextInt();
        System.out.printf("Введите третие число:");
        int threeNamber = scanner.nextInt();
        System.out.println("результат : " + Math.pow((Math.pow(firstNamber, secondNamber)), threeNamber));
    }

    /**
     * 4. Пользователь вводит, две строки. Найти разницу в длине.
     */
    public static void task_4() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите первую строку:");
        String firstString = scanner.nextLine();
        System.out.printf("Введите вторую строку:");
        String secondString = scanner.nextLine();
        double result = firstString.length() - secondString.length();
        if (result < 0) {
            result *= -1;
        }
        System.out.println("результат : " + result);
    }

    /**
     * 5. Пользователь вводит три строки. Найти разницу в длине самой короткой и самой длинной.
     */
    public static void task_5() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите первую строку:");
        String firstString = scanner.nextLine();
        System.out.printf("Введите вторую строку:");
        String secondString = scanner.nextLine();
        System.out.printf("Введите третью строку:");
        String threeString = scanner.nextLine();
        int a = firstString.length();
        int b = secondString.length();
        int c = threeString.length();
        int max = 0;
        int min = 0;
        if (a > b && a > c) {
            max = a;
            min = Math.min(b, c);
        } else if (b > a && b > c) {
            max = b;
            min = Math.min(a, c);
        } else {
            max = c;
            min = Math.min(a, b);
        }
        System.out.println("результат : " + (max - min));
    }

    /**
     * 6. Решить предыдущую задачу, не используя операторы >,< итд.
     */
    public static void task_6() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите первую строку:");
        String firstString = scanner.nextLine();
        System.out.printf("Введите вторую строку:");
        String secondString = scanner.nextLine();
        System.out.printf("Введите третью строку:");
        String threeString = scanner.nextLine();
        int a = firstString.length();
        int b = secondString.length();
        int c = threeString.length();
        int max = Math.max(a, b);
        max = Math.max(max, c);
        int min = Math.min(a, b);
        min = Math.min(min, c);
        System.out.println("результат : " + (max - min));
    }

    /**
     * 7. Сделать калькулятор для трех чисел: пользователь вводит первое, потом оператор, второе, оператор, третье. Посчитать первое на второе, потом результат с третьим. Пример: 11+4*20 Вывод: 300
     */
    public static void task_7() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите первое число:");
        int firstNamber = scanner.nextInt();
        System.out.printf("Введите первое действие :");
        char firstEvent = scanner.next().charAt(0);
        System.out.printf("Введите второе число:");
        int secondNamber = scanner.nextInt();
        System.out.printf("Введите второе действие :");
        char secondEvent = scanner.next().charAt(0);
        System.out.printf("Введите третье число:");
        int threeNamber = scanner.nextInt();
        float stepOne = hendlerMatch(firstNamber, firstEvent, secondNamber);
        System.out.println("результат : " + (hendlerMatch(stepOne, secondEvent, threeNamber)));
    }

    /**
     * 8. Решить предыдущую задачу, но операции считать по приоритету (умножение и деление выше сложения вычитания). Предыдущий пример даст ответ 11 + 80 равно 91
     */
    public static void task_8() {
        float result, stepOne;
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите первое число:");
        int firstNamber = scanner.nextInt();
        System.out.printf("Введите первое действие :");
        char firstEvent = scanner.next().charAt(0);
        System.out.printf("Введите второе число:");
        int secondNamber = scanner.nextInt();
        System.out.printf("Введите второе действие :");
        char secondEvent = scanner.next().charAt(0);
        System.out.printf("Введите третье число:");
        int threeNamber = scanner.nextInt();
        if ((firstEvent != '+' || firstEvent != '-') && (secondEvent == '*' || secondEvent == '/')) {
            stepOne = hendlerMatch(secondNamber, secondEvent, threeNamber);
            result = hendlerMatch(firstNamber, firstEvent, stepOne);
        } else {
            stepOne = hendlerMatch(firstNamber, firstEvent, secondNamber);
            result = hendlerMatch(stepOne, secondEvent, threeNamber);
        }
        System.out.println("результат : " + result);
    }

    protected static float hendlerMatch(float firstNamber, char operatop, float secondNamber) {
        float result = 0;
        if (operatop == '+') {
            result = firstNamber + secondNamber;
        } else if (operatop == '-') {
            result = firstNamber - secondNamber;
        } else if (operatop == '*') {
            result = firstNamber * secondNamber;
        } else if (operatop == '/') {
            if (secondNamber <= 0) {
                System.out.printf("Делить на ноль нельзя");
                return result;
            }
            result = firstNamber / secondNamber;
        }
        return result;
    }


    /**
     * 9. Вывести английский алфавит
     */
    public static void task_9() {
        for (int i = 97; i <= 122; i++) {
            System.out.println((char) i);
        }
    }

    /**
     * 10. Пользователь вводит N. Вывести букву английского алфавита по счету N
     */
    public static void task_10() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите число от 65 - 90 или  97 - 122:");
        int i = scanner.nextInt();
        if (i >= 97 && i <= 122) {
            System.out.println((char) i);
        } else if (i >= 65 && i <= 90) {
            System.out.println((char) i);
        } else {
            System.out.println("Введенное значение не попадает в предлагаемый диапазон");
        }
    }

    /**
     * 11. Вывести случайную букву английского алфавита.
     */
    public static void task_11() {
        boolean flag = true;
        int i;
        while (flag) {
            i = (int) (Math.random() * 100);
            if (i >= 97 && i <= 122) {
                System.out.println((char) i);
                flag = false;
            } else if (i >= 65 && i <= 90) {
                System.out.println((char) i);
                flag = false;
            }
        }
    }

    /**
     * 12. Вывести случайное число от 11 до 22
     */
    public static void task_12() {
            System.out.println((int) (Math.random() * 10 + 11));
    }

}

