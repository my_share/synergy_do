package module_1.lesson_4;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("----------- 1. Составьте программу, которая переводит переменную из byte в short ------------");
        task_1();
        System.out.println("----------- 2. Составьте программу, которая переводит переменную из short в int ------------");
        task_2();
        System.out.println("----------- 3. Составьте программу, которая переводит переменную из int в long ------------");
        task_3();
        System.out.println("----------- 4. Составьте программу, которая переводит переменную из long в int ------------");
        task_4();
        System.out.println("----------- 5. Составьте программу, которая переводит переменную из float в double ------------");
        task_5();
        System.out.println("----------- 6. Составьте программу, которая переводит переменную из double в float ------------");
        task_6();
        System.out.println("----------- 7. Придумайте способ переводить из int в boolean и наоборот. ------------");
        task_7();
        System.out.println("----------- 8. Придумайте способ переводить из String в boolean. ------------");
        task_8();
        System.out.println("----------- 9. Переведите переменную из char в string ------------");
        task_9();
        System.out.println("----------- 10. Переведите переменную из char в int и наоборот. ------------");
        task_10();
        System.out.println("----------- 11. Пусть int равен от 30 до 150. Переведите каждый int в char, и выведите таблицу, где на каждой строчке есть int и соответствующий ему char ------------");
        task_11();
    }

    /**
     * 1. Составьте программу, которая переводит переменную из byte в short
     */
    public static void task_1() {
        byte a = 10;
//       short b = (short) a;
        short b = a;
        System.out.println(b);
    }

    /**
     * 2. Составьте программу, которая переводит переменную из short в int
     */
    public static void task_2() {
        short a = 10;
//       int b = (int) a;
        int b = a;
        System.out.println(b);
    }

    /**
     * 3. Составьте программу, которая переводит переменную из int в long
     */
    public static void task_3() {
        int a = 10;
//       long b = (long) a;
        long b = a;
        System.out.println(b);
    }

    /**
     * 4. Составьте программу, которая переводит переменную из long в int»
     */
    public static void task_4() {
        long a = 10;
        int b = (int) a;
        System.out.println(b);
    }

    /**
     * 5. Составьте программу, которая переводит переменную из float в double
     */
    public static void task_5() {
        float a = 10.0f;
        double b = a;
        System.out.println(b);
    }

    /**
     * 6. Составьте программу, которая переводит переменную из double в float
     */
    public static void task_6() {
        double a = 10.1;
        float b = (float) a;
        System.out.println(b);
    }

    /**
     * 7. Придумайте способ переводить из int в boolean и наоборот.
     */
    public static void task_7() {
        int a = 10;
        boolean b = false;
        if (a > 0) {
            b = true;
        }
        System.out.println(b);
    }

    /**
     * 8. Придумайте способ переводить из String в boolean.
     */
    public static void task_8() {
        String a = "text";
        boolean b = false;
        if (a.length() > 0) {
            b = true;
        }
        System.out.println(b);
    }

    /**
     * 9. Переведите переменную из char в string
     */
    public static void task_9() {
        char a = 64;
        String b = "";
        b = b.valueOf(a);
        System.out.println(b);
    }

    /**
     * 10. Переведите переменную из char в int и наоборот.
     */
    public static void task_10() {
        char a = 64;
        int b = (int) a;
        System.out.println(a);
        System.out.println(b);
        System.out.println((char) b);

    }

    /**
     * 11. Пусть int равен от 30 до 150. Переведите каждый int в char, и выведите таблицу, где на каждой строчке есть int и соответствующий ему char
     */
    public static void task_11() {
        int i = 30;
        while (i <= 150) {
            System.out.println("int: " + i + " -- char: " + (char) i);
            i++;
        }
    }

}

