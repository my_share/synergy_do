package module_1.lesson_3;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("----------- 1. Где хранятся переменные? Сколько переменных можно задать в программе? Чем ограничен размер?------------");
        task_1();
        System.out.println("----------- 2. Пользователь вводит строку, выведите ее длину ------------");
        task_2();
        System.out.println("----------- 3. Пользователь вводит две строки, выведите сумму их длин ------------");
        task_3();
        System.out.println("----------- 4. Пользователь вводит три строки. Найти, какая из них короче всех.------------");
        task_4();
        System.out.println("----------- 5. Пользователь вводит три дробных числа. Вывести те из них, квадратный корень которых меньше 2.------------");
        task_5();
        System.out.println("----------- 6. Пройдите в дебаге программу:\n" +
                "   int x = 1;\n" +
                "   while (x>=-3) {\n" +
                "   System.out.print(x);\n" +
                "   x = x - 1;\n" +
                "   } Сколько раз выводится икс? ------------");
        task_6();
        System.out.println("----------- 7. Пройдите в дебаге программу:\n" +
                "   String str = “Hell”;\n" +
                "   while (str.length()<10) {\n" +
                "   str = str + “o”;\n" +
                "   }\n" +
                "   Сколько раз вызывается метод str.length() ? ------------");
        task_7();
        System.out.println("----------- 8. Пользователь вводит два числа. Разделить меньшее на большее и вывести результат. ------------");
        task_8();
        System.out.println("----------- 9. Пользователь вводит строку. Используя substring, вывести первые 5 символов. ------------");
        task_9();
        System.out.println("----------- 10. Считайте boolean-переменную. Если пользователь ввел true, вывести “истина”, иначе ничего не стоит выводить. ------------");
        task_10();
        System.out.println("----------- 11. Пользователь вводит 3 числа. Сделайте три boolean переменных: есть ли среди введённых четное, есть ли среди введённых отрицательное, есть ли число больше тысячи ------------");
        task_11();
        System.out.println("----------- 12. Пользователь вводит три строки, используя .substring(0, x) выведите эти строки, обрезав те, что длиннее самой короткой. Пример ввода: «повар», «поделка», «лампочка». Вывод: «повар», «подел», «лампо» ------------");
        task_12();
        System.out.println("----------- 13. Мини-игра в слова. Первый игрок вводит слово. Потом второй игрок вводит два числа, с какого по какой символ можно найти слово\n" +
                "    внутри исходного, используя substring. Потом первый игрок вводит два числа. Побеждает тот, чье слово длиннее. Пример:\n" +
                "    1: революционный\n" +
                "    2: 0 3 (вывод: рев)\n" +
                "    1: 7 13 (вывод: ионный)\n" +
                "    Победил игрок 1 ------------");
        task_13();
    }

    /**
     * 1. Где хранятся переменные? Сколько переменных можно задать в программе? Чем ограничен размер?»
     */
    public static void task_1() {
        System.out.println("1.переменный хранятся в оперативной памяти");
        System.out.println("2.бесконечное множество, (одновременное использование может быть вызвано только ограничением памяти");
        System.out.println("3.оперативной памаятью, если ее не хватает данные кладутся в файл подкачик на жесткий диск (тем самым замедляя выполнение программы)");
    }

    /**
     * 2.Пользователь вводит строку, выведите ее длину»
     */
    public static void task_2() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите строку: ");
        String a = scanner.nextLine();
        System.out.print("Введенная строка имеет длинну в колличестве символов равную: " + a.length());
    }

    /**
     * 3. Пользователь вводит две строки, выведите сумму их длин»
     */
    public static void task_3() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите строку один: ");
        String a = scanner.nextLine();
        System.out.print("Введите строку два: ");
        String b = scanner.nextLine();
        System.out.print("Сумму введенных двух строк равна: " + (a.length() + b.length()));
    }

    /**
     * 4. Пользователь вводит три строки. Найти, какая из них короче всех.»
     */
    public static void task_4() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите строку один: ");
        String _a = scanner.nextLine();
        System.out.print("Введите строку два: ");
        String _b = scanner.nextLine();
        System.out.print("Введите строку три: ");
        String _c = scanner.nextLine();
        int a, b, c;
        a = _a.length();
        b = _b.length();
        c = _c.length();
        if (a > b && a > c) {
            System.out.println("Строку один больше остальных  и содержит количество символов равное:" + a);
        } else if (b > c) {
            System.out.println("Строку два больше остальных  и содержит количество символов равное:" + b);
        } else {
            System.out.println("Строку три больше остальных  и содержит количество символов равное:" + c);
        }
    }

    /**
     * 5. Пользователь вводит три дробных числа. Вывести те из них, квадратный корень которых меньше 2.
     */
    public static void task_5() {
        double arNamber[] = new double[3];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Вводя дробные числа,  используй запятую!!!");
        System.out.print("Введите первое дробное числа: ");
        arNamber[0] = scanner.nextDouble();
        System.out.print("Введите второе дробное числа: ");
        arNamber[1] = scanner.nextDouble();
        System.out.print("Введите третье дробное числа: ");
        arNamber[2] = scanner.nextDouble();
        boolean flag = false;
        for (double d : arNamber) {
            if (Math.sqrt(d) < 2) {
                System.out.println("Квадратный корень числа " + d + " < 2");
                flag = true;
            }
        }
        if (!flag) {
            System.out.print("Введенных числах нет числа, корень которого будет < 2");
        }
    }

    /**
     * 6. Пройдите в дебаге программу:.
     */
    public static void task_6() {
        int x = 1;
        while (x >= -3) {
            System.out.print(x);
            x = x - 1;
        }
        System.out.print("\nВыведется 5 раз");
    }

    /**
     * 7. Пройдите в дебаге программу:
     */
    public static void task_7() {
        String str = "Hell";
        while (str.length() < 10) {
            str = str + "o";
        }
        System.out.print("\nВыведется 6 раз");
    }

    /**
     * 8. Пользователь вводит два числа. Разделить меньшее на большее и вывести результат.
     */
    public static void task_8() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите первое число: ");
        int a = scanner.nextInt();
        System.out.print("Введите второе число: ");
        int b = scanner.nextInt();
        if (a > b) {
            System.out.println("Результат деления: " + (a / b));
        } else {
            System.out.println("Результат деления: " + (b / a));
        }
    }

    /**
     * 9. Пользователь вводит строку. Используя substring, вывести первые 5 символов.
     */
    public static void task_9() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите строку: ");
        String str = scanner.nextLine();
        System.out.println(str.substring(0, 5));
    }

    /**
     * 10. Считайте boolean-переменную. Если пользователь ввел true, вывести “истина”, иначе ничего не стоит выводить.
     */
    public static void task_10() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите true или false: ");
        boolean a = scanner.nextBoolean();
        if (a) {
            System.out.println("истина");
        }
    }

    /**
     * 11. Пользователь вводит 3 числа. Сделайте три boolean переменных: есть ли среди введённых четное, есть ли среди введённых отрицательное, есть ли число больше тысячи
     */
    public static void task_11() {
        int[] arNamber = new int[3];
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите первое число: ");
        arNamber[0] = scanner.nextInt();
        System.out.print("Введите второе число: ");
        arNamber[1] = scanner.nextInt();
        System.out.print("Введите второе число: ");
        arNamber[2] = scanner.nextInt();
        boolean _a = false;
        boolean negative = false;
        boolean even = false;
        for (int a : arNamber) {
            if (a < 0) {
                negative = true;
                System.out.println("число отрицательное");
            } else if (a > 1000) {
                _a = true;
                System.out.println("число > 1000");
            } else if (a % 2 == 0) {
                even = true;
                System.out.println("число четное");
            }
        }
    }

    /**
     * 12. Пользователь вводит три строки, используя .substring(0, x) выведите эти строки, обрезав те, что длиннее самой короткой. Пример ввода: «повар», «поделка», «лампочка». Вывод: «повар», «подел», «лампо»
     */
    public static void task_12() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите три слов через запятую: ");
        String a = scanner.nextLine();
        String arStr[] = new String[3];
        arStr = a.split(",");
        int i = arStr[0].length();
        for (String element : arStr) {
            if (element.length() < i) {
                element = element.trim();
                i = element.length();
            }
        }
        for (String element : arStr) {
            System.out.println(element.trim().substring(0, i));
        }
    }

    /**
     * 13. Мини-игра в слова. Первый игрок вводит слово. Потом второй игрок вводит два числа, с какого по какой символ можно найти слово внутри исходного, используя substring. Потом первый игрок вводит два числа. Побеждает тот, чье слово длиннее.
     */
    public static void task_13() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите слово: ");
        String a = scanner.next();
        System.out.print("Игрок 2 введите число от 0 до " + a.length() + ": ");
        int ferstNamber_2 = scanner.nextInt();
        System.out.print("Игрок 2 введите число от 0 до " + a.length() + ": ");
        int secondNamber_2 = scanner.nextInt();
        String newWord_2 = a.substring(ferstNamber_2, secondNamber_2);
        System.out.print("Игрок 1 введите число от 0 до " + a.length() + ": ");
        int ferstNamber_1 = scanner.nextInt();
        System.out.print("Игрок 1 введите число от 0 до " + a.length() + ": ");
        int secondNamber_1 = scanner.nextInt();
        String newWord_1 = a.substring(ferstNamber_1, secondNamber_1);
        if (newWord_1.length() > newWord_2.length()) {
            System.out.println("Победил первый игрок его слово длинее --" + newWord_1 + "--");
        } else {
            System.out.println("Победил второй игрок его слово длинее --" + newWord_2 + "--");
        }
    }
}

