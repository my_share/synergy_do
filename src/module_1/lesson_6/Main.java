package module_1.lesson_6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("----------- 1. Пользователь вводит дробное число. Если оно больше пи, вывести 'pimore' ------------");
        task_1();
        System.out.println("----------- 2. Пользователь вводит строку. Используя метод .contains(‘ ‘) пробел, определите, ввел пользователь одно слово, или больше ------------");
        task_2();
        System.out.println("----------- 3. Пользователь вводит четыре числа. Найти наибольшее из них. ------------");
        task_3();
    }

    /**
     * 1. Пользователь вводит дробное число. Если оно больше пи, вывести 'pimore'
     */
    public static void task_1() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите дробное число:");
        float namber = scanner.nextFloat();
        if (namber > 3.14) {
            System.out.println("вывести 'pimore'");
        } else {
            System.out.println("введеное число меньше ПИ");
        }
    }

    /**
     * 2. Пользователь вводит строку. Используя метод .contains(‘ ‘) пробел, определите, ввел пользователь одно слово, или больше
     */
    public static void task_2() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите строку:");
        String str = scanner.nextLine();
        if (str.contains(" ")) {
            System.out.println("В этой строке больше обного слова");
        } else {
            System.out.println("Это не строка, это одно слово");
        }
    }

    /**
     * 3. Пользователь вводит четыре числа. Найти наибольшее из них.
     */
    public static void task_3() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Введите четыре числа через ';':");
        String str = scanner.nextLine();
        String[] ar = new String[4];
        ar = str.split(";");
        int n = 0;
        for (int i = 0; i < 4; i++) {
            Integer a = new Integer(ar[i]);
            if (n < a){
                n = a;
            }
        }
        System.out.println(n);
    }


}

