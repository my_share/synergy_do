package module_2.lesson_2;

import com.sun.javafx.binding.StringFormatter;
import javafx.scene.image.Image;
import sun.swing.ImageIconUIResource;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("----------- 1.Выведите числа от 0 до миллиона ------------");
        task_1();
        System.out.println("----------- 2.Выведите числа от 1.5 до 101.5: 1.5,2,2.5,3,3.5…101.5 ------------");
        task_2();
        System.out.println("----------- 3.Выведите латинский алфавит от a до z ------------");
        task_3();
        System.out.println("----------- 4.Выведите русский алфавит от а до я ------------");
        task_4();
        System.out.println("----------- 5.Создайте 10 тысяч файлов ------------");
        task_5();
        System.out.println("----------- 6.В файле две строки: секретное слово и подсказка. Вывести подсказку. Считывать строку за строкой, пока игрок не отгадает секретное слово(не введёт его) ------------");
        task_6();
        System.out.println("----------- 7.Предыдущее задание, но если пользователь ввел хотя бы часть слова верно, то писать: горячо. (Проверять с помощью str.contains). ------------");
        task_7();
        System.out.println("----------- 8.Первый игрок сохраняет слово и подсказку в первый файл, второй игрок во второй. Игра начинается, выводятся подсказки, игроки по-очереди пытаются отгадать слово противника. ------------");
        task_8();
        System.out.println("----------- 9.Пока пользователь не введёт строку, содержащую пробел, считывайте строки и выводите их первые буквы ------------");
        task_9();
        System.out.println("----------- 10.Пользователь вводит полный путь и название файла. Пока пользователь не ввел путь к существующему файлу, повторять ввод. Проверить, что файл существует, можно так: File f = new File(); boolean isExists = f.exists(); ------------");
        task_10();
        System.out.println("----------- 11.Сохраните снимки NASA за январь 2022 года ------------");
        task_11();
    }

    /**
     * Выведите числа от 0 до миллиона
     */
    public static void task_1() {
        int i = 0;
        while ((i++) < 1000000) {
            System.out.println(i);
        }
    }

    /**
     * Выведите числа от 1.5 до 101.5: 1.5,2,2.5,3,3.5…101.5
     */
    public static void task_2() {
        double i = 1.5;
        while (i <= 101.5) {
            if (i * 10 % 2 == 0.0) {
                System.out.println((int) i);
            } else {
                System.out.println(i);
            }
            i += 0.5;
        }
    }

    /**
     * Выведите латинский алфавит от a до z
     */
    public static void task_3() {
        char i = 97;
        while (i <= 122) {
            System.out.print(i + " ");
            i++;
        }
    }

    /**
     * Выведите русский алфавит от а до я
     */
    public static void task_4() {
        char i = '\u0430';
        while (i <= '\u044f') {
            System.out.print(i + " ");
            i++;
        }
    }

    /**
     * Создайте 10 тысяч файлов
     */
    public static void task_5() {
        try {
            int i = 1;
            while (i <= 10000) {
                FileWriter fileWriter = new FileWriter("uploud/test_" + i + ".txt");
                fileWriter.close();
                i++;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * В файле две строки: секретное слово и подсказка. Вывести подсказку. Считывать строку за строкой, пока игрок не отгадает секретное слово(не введёт его)
     */
    public static void task_6() {
        try {
            Scanner streamFile = new Scanner(new FileInputStream("uploud/task_6_7.txt"));
            Scanner streamCons = new Scanner(System.in);
            String answerUser = "";
            String question = streamFile.nextLine();
            String answer = streamFile.nextLine();
            System.out.println("Сыграем в игру, мы загадали слово, угадайте, если ответ верный Вы выиграли.\nЗакончить q");
            while (true) {
                if (answerUser.equals(answer) || answerUser.equals("q")) {
                    break;
                }
                System.out.println("Вопрос:" + question);
                System.out.print("Ваш ответ:");
                answerUser = streamCons.nextLine();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Предыдущее задание, но если пользователь ввел хотя бы часть слова верно, то писать: горячо. (Проверять с помощью str.contains).
     */
    public static void task_7() {
        try {
            Scanner streamFile = new Scanner(new FileInputStream("uploud/task_6_7.txt"));
            Scanner streamCons = new Scanner(System.in);
            String answerUser = "";
            String question = streamFile.nextLine();
            String answer = streamFile.nextLine();
            System.out.println("Сыграем в игру, мы загадали слово, угадайте, если ответ верный Вы выиграли.\nЗакончить q");
            while (true) {
                System.out.println("Вопрос:" + question);
                System.out.print("Ваш ответ:");
                answerUser = streamCons.nextLine();

                if (answerUser.equals("q")) {
                    break;
                } else if (answerUser.equals(answer)) {
                    System.out.println("Ответ верный: " + answerUser);
                    break;
                } else if (answerUser.contains(answer.substring(0, 4))) {
                    System.out.println("горячо");
                } else {
                    System.out.println("холодно");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Первый игрок сохраняет слово и подсказку в первый файл, второй игрок во второй. Игра начинается, выводятся подсказки, игроки по-очереди пытаются отгадать слово противника.
     */
    public static void task_8() {
    }

    /**
     * Пока пользователь не введёт строку, содержащую пробел, считывайте строки и выводите их первые буквы
     */
    public static void task_9() {
        Scanner streamCons = new Scanner(System.in);
        while (true) {
            System.out.println("Ведите строку:");
            String str = streamCons.nextLine();
            if (str.contains(" ")) {
                System.out.println("СТОП");
                break;
            } else {
                System.out.println(str.substring(0, 1));
            }
        }
    }

    /**
     * Пользователь вводит полный путь и название файла. Пока пользователь не ввел путь к существующему файлу, повторять ввод. Проверить, что файл существует, можно так: File f = new File(); boolean isExists = f.exists();
     */
    public static void task_10() {
        System.out.println("Подсказка: uploud\\file_one.txt");
        while (true) {
            System.out.print("Ведите путь к файлу:");
            Scanner streamCons = new Scanner(System.in);
            String str = streamCons.nextLine();
            File f = new File(str);
            if (f.exists()) {
                System.out.println("удача");
                break;
            } else {
                System.out.print("попробуй еще раз:");
            }
        }
    }

    /**
     * Сохраните снимки NASA за январь 2022 года
     */
    public static void task_11() {
        String url = "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&start_date=2022-01-01&end_date=2022-01-30";
        String[] arData = new String[30];
        arData = (getPageSourse(url)).split("},");
        for (String element:arData) {
            savePicture(getUrl(element),getDate(element));
        }
    }

    private static void savePicture(String url, String name) {
        try{
            InputStream is = new URL(url).openStream();
            Files.copy(is, Paths.get("uploud\\task_2_11\\"+name+getExpansion(url)));
        }catch (Exception e){
            System.out.println("ошибка!!!" + e.getMessage());
        }
    }

    /**
     * полученеи web страницы
     *
     * @param url
     * @return
     */
    private static String getPageSourse(String url) {
        StringBuilder result = new StringBuilder();
        String line;

        try {
            URLConnection urlConnection = new URL(url).openConnection();
            urlConnection.addRequestProperty("User-Agent", "Mozilla");
            urlConnection.setReadTimeout(5000);
            urlConnection.setConnectTimeout(5000);

            InputStream is = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                result.append(line);
            }
            is.close();
            br.close();
        } catch (Exception e) {
            System.out.println("ошибка!!!" + e.getMessage());
        }
        return result.toString();
    }

    /**
     *
     * @param data
     * @return
     */
    private static String getUrl(String data){
        int  indexOf = data.lastIndexOf("url");
        String newStr = data.substring(indexOf+6);
        int  lastIndexOf = newStr.indexOf("\"");
        return (data.substring(indexOf+6,indexOf+6+lastIndexOf));
    }

    /**
     *
     * @param data
     * @return
     */
    private static String getDate(String data){
        int  indexOf = data.lastIndexOf("date");
        String newStr = data.substring(indexOf+6);
        int  lastIndexOf = newStr.indexOf(",");
        return data.substring(indexOf+6,indexOf+6+lastIndexOf).replace("\"","");
    }

    private static String getExpansion(String str){
        return str.substring(str.lastIndexOf('.'));
    }
}